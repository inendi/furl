/*
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 * Version 2, December 2004 
 *
 * Copyright (C) 2012 Sebastien Tricaud <sebastien@honeynet.org> 
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

#ifndef _FURL_ERRORS_H_
#define _FURL_ERRORS_H_

#define FURL_URL_EMPTY 1
#define FURL_URL_TOOLONG 2

#define FURL_URL_MEM_ERROR 3
#define FURL_URL_PARSER_ERROR 4
#define FURL_URL_UNKNOWN_ERROR 5

#endif	/* _FURL_ERRORS_H_ */
